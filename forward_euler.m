function [new_w] = forward_euler(w, t, problem, dt)
  % [new_w] = forward_euler(w, t, problem, dt) es el método de Euler
  % explícito.
  %
  % ¡¡¡¡¡¡¡¡¡¡¡COMPLETAR!!!!!!!!!!!!
  % - Inputs:
  % w : Vector de estado. Es un vector columna. La primera componente es la
  % posición según el eje X. La segunda es... Las unidades son...
  % t : Tiempo en el que se evalúa la derivada temporal del vector estado
  % [s].
  % problem : función anónima que llama al problema en cuestión. Sus inputs
  % son el vector de estado, w, y el tiempo en el que se evalúa la derivada
  % temporal del vector estado, t. Se pide que devuelva f_0, que es la
  % derivada temporal evaluada en el instante t.
  % dt : Incremento temporal dado en cada paso de cálculo [s].
  %
  % - Outputs:
  % new_w : Nuevo vector de estado correspondiente al siguiente paso
  % temporal. Tiene la misma estructura que el vector de estado, w.
  
  % Se obtiene la derivada temporal del vector estado, f_0, evaluada en el
  % instante t para el problema:
  [f_0, ~, ~] = problem(w, t);
  
  % Cálculo del nuevo vector de estado mediante el método de Euler
  % explícito:
  new_w = w + dt*f_0;
  
end

