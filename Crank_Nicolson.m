function [new_w] = Crank_Nicolson(w, t, problem, dt)
  % [new_w] = Crank_Nicolson(w, t, problem, dt) es un integrador que sigue
  % el método de Crank-Nicolson.
  % 
  % ¡¡¡¡¡¡¡¡¡COMPLETAR!!!!!!!!!
  % - Inputs:
  %
  %
  % - Outputs:
  %
  %
  
  % ¡¡¡¡¡¡¡¡¡COMENTAR!!!!!!!!!
  [f_0, A_0, df_dt_0] = problem(w, t);
  
  I = eye(size(A_0));
  B = (I - dt/2*A_0);
  c = w + dt*(f_0 - 1/2*A_0*w + dt/2*df_dt_0);
  
  new_w = linsolve(B, c);
  
end

