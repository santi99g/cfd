function [new_w] = backward_euler(w, t, problem, dt)
  % [new_w] = backward_euler(w, t, problem, dt) es el método de Euler
  % implícito.
  %
  % ¡¡¡¡¡¡¡¡¡¡¡COMPLETAR!!!!!!!!!!!!
  % - Inputs:
  % w : Vector de estado. Es un vector columna. La primera componente es la
  % posición según el eje X. La segunda es... Las unidades son...
  % t : Tiempo en el que se evalúa la derivada temporal del vector estado
  % [s].
  % problem : función anónima que llama al problema en cuestión. Sus inputs
  % son el vector de estado, w, y el tiempo en el que se evalúa la derivada
  % temporal del vector estado, t. Se pide que devuelva f_0, que es la
  % derivada temporal evaluada en el instante t.
  % dt : Incremento temporal dado en cada paso de cálculo [s].
  %
  % - Outputs:
  % new_w : Nuevo vector de estado correspondiente al siguiente paso
  % temporal. Tiene la misma estructura que el vector de estado, w.
  
  %%%%%% ¡¡¡COMPLETAR COMENTARIOS!!!
  % Se obtienen la derivada temporal, f_0, evaluada en el instante t para 
  % el problema; la matric jacobiana, A_0; y la derivada temporal..., 
  % df_dt_0:
  [f_0, A_0, df_dt_0] = problem(w, t);
  
  % Determinación de las matrices que conforman el sistema a resolver.
  % Donde I es la matriz identidad de tamaño del jacobiano; B es la matriz
  % que premultiplica a la incógnita (new_w); y c es la matriz del término
  % independiente:
  I = eye(size(A_0));
  B = (I - dt*A_0);
  c = w + dt*(f_0 - A_0*w + dt*df_dt_0);
  
  % Cálculo del nuevo vector de estado, new_w, mediante la resolución de un
  % sistema matricial lineal correspondiente al método de Euler explícito:
  new_w = linsolve(B, c);
  
end

